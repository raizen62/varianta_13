import React from 'react';

export default class AddVacation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            destination: '',
            locationType: '',
            price: 0
        };
    }

    handleDestination = (el) => {
        this.setState({
            destination: el.target.value
        })
    }

    handlelocationType = (el) => {
        this.setState({
            locationType: el.target.value
        })
    }

    handlePrice = (el) => {
        this.setState({
            price: el.target.value
        })
    }

    render() {
        return (
            <div>
                <input id="vacation-destination" onChange={this.handleDestination} name="vacation-destination" />
                <input id="vacation-location-type" onChange={this.handlelocationType} name="vacation-location-type" />
                <input id="vacation-price" onChange={this.handlePrice} name="vacation-price" />
                <button value="add vacation" onClick={this.handleAdd}></button>
            </div>
        );
    }

    handleAdd = () => {
        let item = { ...this.state };
        this.props.itemAdded(item);
    }
}