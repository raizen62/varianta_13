import React from 'react';
import AddVacation from './AddVacation';
export default class BookList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
        this.addItem = (car) => {
            this.setState(prevState => ({
                data: [...prevState.data, car]
            }));
            console.log(this.state);
        }
    }

    render() {
        return (
            <div>
                <AddVacation itemAdded={this.addItem} />
            </div>
        );
    }
}